#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

int max(int num1,int num2) {
    return num1 > num2 ? num1 : num2;
}

int max_sum_no_adjacent(int *array,int length) {
    if(length == 1)
        return array[0];
    int *sol = (int *) malloc(length * sizeof(int));
    sol[0] = array[0];
    sol[1] = array[1];

    for(int i=2;i<length;i++) {
        sol[i] = max(sol[i-1],sol[i-2] + array[i]);
    }

    return sol[length - 1];
}

int main() {
    int arr1[] = {1,2,3,4};
    printf("max_sum_no_adjacent(arr1,4) : %d\n",max_sum_no_adjacent(arr1,4));
    return 0;
}
