#include <iostream>
#include <algorithm>

struct Item {
    double price;
    double weight;
};

void debug(Item* items,int total_items);
double fraction_knapsack(Item* items,int total_items,int knapsack_capacity);

int main() {
    int total_items, knapsack_capacity;
    std::cout<<"Total Number of Items : "; std::cin>>total_items;
    std::cout<<"Knapsack capacity : "; std::cin>>knapsack_capacity;

    Item *items = new Item[total_items];
    std::cout<<"Enter all items with price and weight\n";
    for(int i=0;i<total_items;i++) {
        double price,weight; std::cin>>price>>weight;
        items[i].price = price;
        items[i].weight = weight;
    }

    // Sort the array by price / weight ratio
    std::sort(items, items + total_items,
            [](Item const &item1 , Item const &item2) -> bool {
                return ( (item1.price / item1.weight) > (item2.price / item2.weight) );
            });

    std::cout<<fraction_knapsack(items,total_items,knapsack_capacity)<<"\n";

    return 0;
}

void debug(Item *items,int total_items) {
    for(int i=0;i<total_items;i++) {
        std::cout<<items[i].price<<" "<<items[i].weight<<"\n";
    }
}


double fraction_knapsack(Item* items,int total_items,int knapsack_capacity) {
    int current_max_weight = 0;
    int max_profit = 0;
    for(int i=0;i<total_items;i++) {
        if(current_max_weight + items[i].weight < knapsack_capacity) {
            max_profit += items[i].price;
            current_max_weight += items[i].weight;
        } else {
            int remain_weight = knapsack_capacity - current_max_weight;
            max_profit += (remain_weight/items[i].weight)*(items[i].price);
            break;
        }
    }
    return max_profit;
}
